package com.dev.d1l.healthyfood;

import android.support.v4.content.res.TypedArrayUtils;

import com.dev.d1l.healthyfood.models.request.LoginRequest;
import com.dev.d1l.healthyfood.models.request.SignUpRequest;
import com.dev.d1l.healthyfood.models.response.CategoriesResponse;
import com.dev.d1l.healthyfood.models.response.DishesResponse;
import com.dev.d1l.healthyfood.utils.ApiInterface;
import com.dev.d1l.healthyfood.utils.RestService;

import org.junit.Before;
import org.junit.Test;

import retrofit2.Call;
import retrofit2.Response;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    private ApiInterface apiService;
    private LoginRequest request;
    private LoginRequest falseRequest;
    private SignUpRequest signUpRequest;

    @Before
    public void setUp() throws Exception {
        apiService = RestService.getInstance();

        request = new LoginRequest();
        request.setUsername("Admin");
        request.setPassword("bsuirhealthproject");

        falseRequest = new LoginRequest();
        falseRequest.setUsername("Admin");
        falseRequest.setPassword("incorrectpassword");

        signUpRequest = new SignUpRequest();
        signUpRequest.setPhone("834298932");
        signUpRequest.setThighGirth(12f);
        signUpRequest.setWristGirth(12f);
        signUpRequest.setWaistGirth(12f);
        signUpRequest.setConfirmPassword("password");
        signUpRequest.setPassword("password");
        signUpRequest.setDestinationWeight(99f);
        signUpRequest.setSexName("Male");
        signUpRequest.setDateOfBirth("12/12/1994");
        signUpRequest.setEmail("sasha23@gmail.com");
        signUpRequest.setHeight(154f);
        signUpRequest.setFirstname("firstname");
        signUpRequest.setLastname("lastname");
        signUpRequest.setUserActivityType("low");
        signUpRequest.setWeight(105f);
    }

    @Test
    public void testLoginResponse() throws Exception {
        Call<String> call = apiService.login(request);
        Response<String> response = call.execute();
        assertEquals(200, response.code());
        assertEquals(null, response.body());
        assertTrue(response.isSuccessful());
    }

    @Test
    public void testFalseLoginResponse() throws Exception {
        Call<String> call = apiService.login(falseRequest);
        Response<String> response = call.execute();
        assertEquals(400, response.code());
        assertEquals(null, response.body());
        assertFalse(response.isSuccessful());
    }

    @Test
    public void testDishesResponse() throws Exception {
        Call<DishesResponse> call = apiService.getDishes();
        Response<DishesResponse> response = call.execute();
        assertEquals(200, response.code());
        assertNotEquals(null, response.body());
        assertNotEquals(0, response.body().getDishes().size());
        assertTrue(response.isSuccessful());
    }

    @Test
    public void testCategoriesResponse() throws Exception {
        Call<CategoriesResponse> call = apiService.getCategories();
        Response<CategoriesResponse> response = call.execute();
        assertEquals(200, response.code());
        assertNotEquals(null, response.body());
        assertNotEquals(0, response.body().getCategories().size());
        assertTrue(response.isSuccessful());
    }

    @Test
    public void testSignUpResponse() throws Exception {
        Call<String> call = apiService.signUp(signUpRequest);
        Response<String> response = call.execute();
        assertEquals(200, response.code());
        assertEquals(null, response.body());
        assertTrue(response.isSuccessful());
    }

    @Test
    public void testFalseSignUpResponse() throws Exception {
        signUpRequest.setPassword("");
        Call<String> call = apiService.signUp(signUpRequest);
        Response<String> response = call.execute();
        assertEquals(400, response.code());
        assertEquals(null, response.body());
        assertNotEquals(null, response.errorBody());
        assertFalse(response.isSuccessful());
    }
}