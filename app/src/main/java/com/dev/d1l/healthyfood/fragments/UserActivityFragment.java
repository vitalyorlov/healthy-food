package com.dev.d1l.healthyfood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.d1l.healthyfood.R;

/**
 * Created by d1l on 5/8/17.
 */

public class UserActivityFragment extends Fragment {

    public UserActivityFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_activity, container, false);

        return v;
    }
}
