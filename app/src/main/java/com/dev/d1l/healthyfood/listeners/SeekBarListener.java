package com.dev.d1l.healthyfood.listeners;

import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by d1l on 5/2/17.
 */

public class SeekBarListener implements SeekBar.OnSeekBarChangeListener {
    private SeekBar mSeekBar;
    private TextView mTextView;
    private String additionalInfo;

    public SeekBarListener(SeekBar mSeekBar, TextView mTextView, String additionalInfo,
                           Integer minValue, Integer maxValue) {
        this.mSeekBar = mSeekBar;
        this.mTextView = mTextView;

        Integer max = (maxValue - minValue);
        this.mSeekBar.setMax(max);

        mTextView.setText(String.valueOf(mSeekBar.getProgress()) + additionalInfo);
        this.additionalInfo = additionalInfo;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mTextView.setText(String.valueOf(mSeekBar.getProgress()) + additionalInfo);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mTextView.setText(String.valueOf(mSeekBar.getProgress()) + additionalInfo);
    }
}
