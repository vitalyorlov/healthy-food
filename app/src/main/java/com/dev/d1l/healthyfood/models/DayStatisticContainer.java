package com.dev.d1l.healthyfood.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by d1l on 6/14/17.
 */

public class DayStatisticContainer implements Serializable {
    private List<DayStatistic> dayStatisticList;

    public List<DayStatistic> getDayStatisticList() {
        return dayStatisticList;
    }

    public void setDayStatisticList(List<DayStatistic> dayStatisticList) {
        this.dayStatisticList = dayStatisticList;
    }

    public DayStatisticContainer(List<DayStatistic> dayStatisticList) {
        this.dayStatisticList = dayStatisticList;
    }

    public DayStatisticContainer() {
    }
}
