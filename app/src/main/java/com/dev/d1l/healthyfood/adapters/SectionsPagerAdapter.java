package com.dev.d1l.healthyfood.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dev.d1l.healthyfood.MainActivity;
import com.dev.d1l.healthyfood.fragments.PlaceholderFragment;
import com.dev.d1l.healthyfood.models.DayStatistic;

import java.util.List;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private static final int PAGE_COUNT = 60;
    private List<DayStatistic> dayStatistics;

    public SectionsPagerAdapter(FragmentManager fm, List<DayStatistic> dayStatistics) {
        super(fm);
        this.dayStatistics = dayStatistics;
    }

    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance(dayStatistics.get(position));
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
