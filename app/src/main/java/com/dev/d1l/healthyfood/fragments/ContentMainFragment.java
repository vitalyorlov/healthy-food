package com.dev.d1l.healthyfood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dev.d1l.healthyfood.R;
import com.dev.d1l.healthyfood.adapters.SectionsPagerAdapter;
import com.dev.d1l.healthyfood.constants.SharedPrefParams;
import com.dev.d1l.healthyfood.models.DayStatistic;
import com.dev.d1l.healthyfood.models.DayStatisticContainer;
import com.dev.d1l.healthyfood.models.request.LoginRequest;
import com.dev.d1l.healthyfood.tasks.SaveDataTask;
import com.dev.d1l.healthyfood.utils.ApiInterface;
import com.dev.d1l.healthyfood.utils.CacheService;
import com.dev.d1l.healthyfood.utils.RestService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by d1l on 5/8/17.
 */

public class ContentMainFragment extends Fragment implements Callback<List<DayStatistic>> {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    public ContentMainFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_main, container, false);

        List<DayStatistic> dayStatistics = getDayStatistics();
        completeList(dayStatistics);

        initViewPager(v, dayStatistics);

        return v;
    }

    private void initViewPager(View v, List<DayStatistic> dayStatistics) {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager(), dayStatistics);

        mViewPager = (ViewPager) v.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(30);
    }

    private List<DayStatistic> getDayStatistics() {
        DayStatisticContainer dsContainer = (DayStatisticContainer)
                CacheService.loadData(this.getActivity(), SharedPrefParams.STATISTICS, new DayStatisticContainer());

        if (dsContainer.getDayStatisticList() != null) {
            return dsContainer.getDayStatisticList();
        } else {
            ApiInterface apiService = RestService.getInstance();

            LoginRequest credentials = (LoginRequest)
                    CacheService.loadData(this.getActivity(), SharedPrefParams.CREDENTIALS, new LoginRequest());

            Call<List<DayStatistic>> call = apiService.getDaysStatistics("application/json", credentials);
            call.enqueue(this);

            return new ArrayList<>();
        }
    }

    private void completeList(List<DayStatistic> dayStatistics) {
        if (dayStatistics == null) {
            dayStatistics = new ArrayList<>();
        }
        for (int i = dayStatistics.size(); i <= 60; i++) {
            dayStatistics.add(new DayStatistic());
        }
    }

    @Override
    public void onResponse(Call<List<DayStatistic>> call, Response<List<DayStatistic>> response) {
        int code = response.code();
        if (code == 200) {
            List<DayStatistic> dsList = response.body();

            SaveDataTask task = new SaveDataTask(this.getActivity(),
                    SharedPrefParams.STATISTICS, new DayStatisticContainer(dsList));
            task.execute();

            completeList(dsList);
            initViewPager(getView(), dsList);
        }
    }

    @Override
    public void onFailure(Call<List<DayStatistic>> call, Throwable t) {
        Toast.makeText(this.getActivity(), "Error getting statistic. Check your network connection", Toast.LENGTH_LONG).show();
    }
}
