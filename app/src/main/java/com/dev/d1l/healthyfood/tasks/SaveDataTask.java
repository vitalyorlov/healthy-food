package com.dev.d1l.healthyfood.tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dev.d1l.healthyfood.utils.CacheService;

import java.io.Serializable;


public class SaveDataTask extends AsyncTask<Void, Void, Void> {

    private Activity context;
    private String key;
    private Serializable value;

    public SaveDataTask(Activity context, String key, Serializable value) {
        this.context = context;
        this.key = key;
        this.value = value;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        CacheService.saveData(context, key, value);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }
}
