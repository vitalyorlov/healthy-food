package com.dev.d1l.healthyfood.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.d1l.healthyfood.MainActivity;
import com.dev.d1l.healthyfood.R;
import com.dev.d1l.healthyfood.models.DayStatistic;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

public class PlaceholderFragment extends Fragment {

    private DayStatistic dayStatistic;
    PieChart mFoodChart;
    PieChart mWaterChart;
    PieChart mProteinsChart;
    PieChart mFatsChart;
    PieChart mCarbohydratesChart;

    private int[] yValues = {21, 2, 2};
    public static final int[] MY_COLORS = {
            Color.rgb(84,124,101), Color.rgb(64,64,64), Color.rgb(64,64,64),
            Color.rgb(38,40,53), Color.rgb(215,60,55)
    };

    public PlaceholderFragment() {
    }

    public static PlaceholderFragment newInstance(DayStatistic dayStatistic) {
        final PlaceholderFragment fragment = new PlaceholderFragment();

        fragment.init(dayStatistic);

        return fragment;
    }

    private void init(DayStatistic dayStatistic) {
        this.dayStatistic = dayStatistic;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mFoodChart = (PieChart) rootView.findViewById(R.id.foodChart);
        mWaterChart = (PieChart) rootView.findViewById(R.id.waterChart);
        mProteinsChart = (PieChart) rootView.findViewById(R.id.proteinsChart);
        mFatsChart = (PieChart) rootView.findViewById(R.id.fatsChart);
        mCarbohydratesChart = (PieChart) rootView.findViewById(R.id.carbohydratesChart);

        initChart(mFoodChart, false, "85%");
        initChart(mWaterChart, true, "68%");
        initChart(mProteinsChart, true, "78");
        initChart(mFatsChart, true, "82");
        initChart(mCarbohydratesChart, true, "46");

        setDataForPieChart(mFoodChart);
        setDataForPieChart(mWaterChart);
        setDataForPieChart(mProteinsChart);
        setDataForPieChart(mFatsChart);
        setDataForPieChart(mCarbohydratesChart);

        return rootView;
    }

    private void initChart(PieChart mChart, boolean isSmall, String initText) {
        mChart.setCenterText(initText);
        mChart.setCenterTextColor(Color.WHITE);
        if (isSmall) {
            mChart.setCenterTextSize(13);
        } else {
            mChart.setCenterTextSize(16);
        }
        mChart.setHoleColor(Color.TRANSPARENT);
        mChart.setDescription(null);
        mChart.setHoleRadius(90);
        mChart.setTransparentCircleRadius(100);
        Legend legend = mChart.getLegend();
        legend.setEnabled(false);
    }

    public void setDataForPieChart(PieChart mChart) {
        ArrayList<PieEntry> yVals1 = new ArrayList<PieEntry>();

        for (int i = 0; i < yValues.length; i++)
            yVals1.add(new PieEntry(yValues[i], i));

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(0);
        dataSet.setSelectionShift(2);

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : MY_COLORS)
            colors.add(c);
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);

        data.setValueFormatter(new DefaultValueFormatter(3));
        data.setValueTextSize(0f);
        data.setValueTextColor(Color.WHITE);

        mChart.setData(data);
        mChart.highlightValues(null);

        mChart.invalidate();

        mChart.animateXY(1400, 1400);

    }
}
