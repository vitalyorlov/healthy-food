package com.dev.d1l.healthyfood.interceptors;

import com.dev.d1l.healthyfood.models.Cook;
import com.dev.d1l.healthyfood.tasks.SaveDataTask;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by d1l on 6/14/17.
 */

public class ReceivedCookiesInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            HashSet<String> cookies = new HashSet<>();

            for (String header : originalResponse.headers("Set-Cookie")) {
                cookies.add(header);
            }

            Cook.cookie = cookies;
        }

        return originalResponse;
    }
}
