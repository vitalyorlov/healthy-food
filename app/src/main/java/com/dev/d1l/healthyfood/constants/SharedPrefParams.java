package com.dev.d1l.healthyfood.constants;

/**
 * Created by d1l on 5/2/17.
 */

public class SharedPrefParams {
    public static final String CREDENTIALS = "credentials";
    public static final String CATEGORIES = "categories";
    public static final String DISHES = "dishes";
    public static final String USER_DATA = "user_data";
    public static final String STATISTICS = "statistics";
}
