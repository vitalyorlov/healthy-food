package com.dev.d1l.healthyfood;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.dev.d1l.healthyfood.adapters.DishesRvAdapter;
import com.dev.d1l.healthyfood.adapters.DishesSectionsPagerAdapter;
import com.dev.d1l.healthyfood.adapters.RationDishesRvAdapter;
import com.dev.d1l.healthyfood.adapters.SectionsPagerAdapter;
import com.dev.d1l.healthyfood.constants.SharedPrefParams;
import com.dev.d1l.healthyfood.models.Category;
import com.dev.d1l.healthyfood.models.Dish;
import com.dev.d1l.healthyfood.models.request.LoginRequest;
import com.dev.d1l.healthyfood.models.response.CategoriesResponse;
import com.dev.d1l.healthyfood.models.response.DishesResponse;
import com.dev.d1l.healthyfood.tasks.SaveDataTask;
import com.dev.d1l.healthyfood.utils.ApiInterface;
import com.dev.d1l.healthyfood.utils.CacheService;
import com.dev.d1l.healthyfood.utils.RestService;

import net.yanzm.mth.MaterialTabHost;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RationActivity extends AppCompatActivity {

    private RecyclerView rv;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ration);

        rv = (RecyclerView) findViewById(R.id.ration_dishes_rv);

        mLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(mLayoutManager);

        DishesResponse dishesResponse = (DishesResponse)
                CacheService.loadData(this, SharedPrefParams.DISHES, new DishesResponse());

        mAdapter = new RationDishesRvAdapter(dishesResponse.getDishes(), this);
        rv.setAdapter(mAdapter);
        rv.setHasFixedSize(true);
    }

}
