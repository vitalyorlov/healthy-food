package com.dev.d1l.healthyfood.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.dev.d1l.healthyfood.R;

import java.io.Serializable;
import java.util.ArrayList;

public class CacheService {
    static SharedPreferences sPref;

    public static void saveData(Activity context, String key, Serializable value) {
        sPref = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(key, ObjectSerializer.serialize(value));
        ed.commit();
    }

    public static Object loadData(Activity context, String key, Serializable value) {
        sPref = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        return ObjectSerializer.deserialize(
                sPref.getString(key, ObjectSerializer.serialize(value)));
    }
}
