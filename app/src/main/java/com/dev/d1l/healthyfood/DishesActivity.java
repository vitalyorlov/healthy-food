package com.dev.d1l.healthyfood;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import com.dev.d1l.healthyfood.adapters.DishesSectionsPagerAdapter;
import com.dev.d1l.healthyfood.adapters.SectionsPagerAdapter;
import com.dev.d1l.healthyfood.constants.SharedPrefParams;
import com.dev.d1l.healthyfood.models.Category;
import com.dev.d1l.healthyfood.models.Dish;
import com.dev.d1l.healthyfood.models.request.LoginRequest;
import com.dev.d1l.healthyfood.models.response.CategoriesResponse;
import com.dev.d1l.healthyfood.models.response.DishesResponse;
import com.dev.d1l.healthyfood.tasks.SaveDataTask;
import com.dev.d1l.healthyfood.utils.ApiInterface;
import com.dev.d1l.healthyfood.utils.CacheService;
import com.dev.d1l.healthyfood.utils.RestService;

import net.yanzm.mth.MaterialTabHost;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DishesActivity extends AppCompatActivity
        implements Callback<CategoriesResponse> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dishes);

        //initTabHost(new CategoriesResponse());

        CategoriesResponse categoriesResponse = (CategoriesResponse)
                CacheService.loadData(this, SharedPrefParams.CATEGORIES, new CategoriesResponse());

        if (categoriesResponse != null && categoriesResponse.getCategories() != null) {
            initTabHost(categoriesResponse);
        } else {
            ApiInterface apiService = RestService.getInstance();
            Call<CategoriesResponse> call = apiService.getCategories();
            call.enqueue(this);
        }

    }

    private void initTabHost(CategoriesResponse categoriesResponse) {
        MaterialTabHost tabHost = (MaterialTabHost) findViewById(R.id.categories);
        tabHost.setType(MaterialTabHost.Type.Centered);

        if (categoriesResponse.getCategories() == null) {
            categoriesResponse = getEmptyCategoriesResponse();
        }

        DishesSectionsPagerAdapter pagerAdapter = new DishesSectionsPagerAdapter(getSupportFragmentManager(),
                categoriesResponse.getCategories().size(), categoriesResponse.getCategories());
        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            tabHost.addTab(pagerAdapter.getPageTitle(i));
        }
        final ViewPager viewPager = (ViewPager) findViewById(R.id.dishes_container);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(tabHost);

        tabHost.setOnTabChangeListener(new MaterialTabHost.OnTabChangeListener() {
            @Override
            public void onTabSelected(int position) {
                viewPager.setCurrentItem(position);
            }
        });
    }

    @Override
    public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
        int code = response.code();
        if (code == 200) {
            CategoriesResponse categoriesResponse = response.body();
            SaveDataTask task = new SaveDataTask(getActivity(),
                    SharedPrefParams.CATEGORIES, categoriesResponse);
            task.execute();
            initTabHost(categoriesResponse);
        } else {
            Toast.makeText(this, "Error getting categories: " + String.valueOf(code), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<CategoriesResponse> call, Throwable t) {
        Toast.makeText(this, "Connection error", Toast.LENGTH_LONG).show();
    }

    public Activity getActivity() {
        return this;
    }

    public CategoriesResponse getEmptyCategoriesResponse() {
        CategoriesResponse response = new CategoriesResponse();
        response.setCategories(new ArrayList<Category>());
        return response;
    }
}
