package com.dev.d1l.healthyfood.models.response;

import com.dev.d1l.healthyfood.models.Category;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by d1l on 5/1/17.
 */

public class CategoriesResponse implements Serializable {
    @SerializedName("@odata.context")
    private String context;
    @SerializedName("value")
    private List<Category> categories;

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
