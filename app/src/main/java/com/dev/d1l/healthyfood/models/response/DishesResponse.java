package com.dev.d1l.healthyfood.models.response;

import com.dev.d1l.healthyfood.models.Category;
import com.dev.d1l.healthyfood.models.Dish;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by d1l on 5/1/17.
 */

public class DishesResponse implements Serializable {
    @SerializedName("@odata.context")
    private String context;
    @SerializedName("value")
    private List<Dish> dishes;

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }
}
