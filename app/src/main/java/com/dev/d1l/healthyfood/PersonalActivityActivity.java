package com.dev.d1l.healthyfood;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.d1l.healthyfood.models.request.LoginRequest;
import com.dev.d1l.healthyfood.models.request.SignUpRequest;
import com.dev.d1l.healthyfood.utils.ApiInterface;
import com.dev.d1l.healthyfood.utils.RestService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

public class PersonalActivityActivity extends AppCompatActivity  implements Callback<String> {
    private SignUpRequest request;
    private View mProgressView;
    private View mLoginFormView;
    private Button mSignUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_activity);

        Intent intent = getIntent();
        request = (SignUpRequest) intent.getSerializableExtra("request");

        ImageButton mBackButton = (ImageButton) findViewById(R.id.back_button);
        mBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PersonalActivityActivity.super.onBackPressed();
            }
        });

        RadioButton activityVeryLowButton = (RadioButton)findViewById(R.id.activity_very_low);
        activityVeryLowButton.setOnClickListener(radioButtonClickListener);
        RadioButton activityLowButton = (RadioButton)findViewById(R.id.activity_low);
        activityLowButton.setOnClickListener(radioButtonClickListener);
        RadioButton activityMiddleButton = (RadioButton)findViewById(R.id.activity_middle);
        activityMiddleButton.setOnClickListener(radioButtonClickListener);

        mSignUpButton = (Button) findViewById(R.id.sign_up_button);
        mSignUpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignUp();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    View.OnClickListener radioButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RadioButton rb = (RadioButton)v;
            switch (rb.getId()) {
                case R.id.activity_very_low: request.setUserActivityType("Very Low");
                    break;
                case R.id.activity_low: request.setUserActivityType("Low");
                    break;
                case R.id.activity_middle: request.setUserActivityType("Middle");
                    break;

                default:
                    break;
            }
        }
    };

    private void attemptSignUp() {
        boolean cancel = false;
        View focusView = null;

        if (cancel) {
            focusView.requestFocus();
        } else {
            mSignUpButton.setEnabled(false);
            showProgress(true);

            ApiInterface apiService = RestService.getInstance();

            Call<String> call = apiService.signUp(request);
            call.enqueue(this);
        }
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {
        mSignUpButton.setEnabled(true);
        showProgress(false);

        int code = response.code();
        if (code == 200) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Error sign up: " + String.valueOf(code), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        mSignUpButton.setEnabled(true);
        showProgress(false);
        Toast.makeText(this, "Error sign up: " + t.getMessage(), Toast.LENGTH_LONG).show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

