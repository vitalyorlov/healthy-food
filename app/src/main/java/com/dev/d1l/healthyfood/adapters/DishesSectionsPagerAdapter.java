package com.dev.d1l.healthyfood.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dev.d1l.healthyfood.fragments.DishesFragment;
import com.dev.d1l.healthyfood.fragments.PlaceholderFragment;
import com.dev.d1l.healthyfood.models.Category;

import java.util.List;

public class DishesSectionsPagerAdapter extends FragmentPagerAdapter {

    private int categoriesCount;
    private List<Category> categories;

    public DishesSectionsPagerAdapter(FragmentManager fm, int categoriesCount, List<Category> categories) {
        super(fm);
        this.categoriesCount = categoriesCount;
        this.categories = categories;
    }

    @Override
    public Fragment getItem(int position) {
        return new DishesFragment();
    }

    @Override
    public int getCount() {
        return categoriesCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categories.get(position).getName();
    }
}
