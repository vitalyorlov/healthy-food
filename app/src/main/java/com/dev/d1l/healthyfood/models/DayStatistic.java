package com.dev.d1l.healthyfood.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by d1l on 6/13/17.
 */

public class DayStatistic implements Serializable {

    @SerializedName("KiloсaloriesAmountToConsume")
    private Float kiloсaloriesAmountToConsume;
    @SerializedName("KiloсaloriesConsumed")
    private Float kiloсaloriesConsumed;
    @SerializedName("WaterAmountToConsume")
    private Float waterAmountToConsume;
    @SerializedName("WaterConsumed")
    private Float waterConsumed;
    @SerializedName("ProteinAmountToConsume")
    private Float proteinAmountToConsume;
    @SerializedName("ProteinConsumed")
    private Float proteinConsumed;
    @SerializedName("CarbAmountToConsume")
    private Float carbAmountToConsume;
    @SerializedName("CarbsConsumed")
    private Float carbsConsumed;
    @SerializedName("FatAmountToConsume")
    private Float fatAmountToConsume;
    @SerializedName("FatConsumed")
    private Float fatConsumed;
    @SerializedName("Date")
    private String date;
    @SerializedName("IsRegistered")
    private Boolean isRegistered;
    @SerializedName("IsEditable")
    private Boolean isEditable;

    public Float getKiloсaloriesAmountToConsume() {
        return kiloсaloriesAmountToConsume;
    }

    public void setKiloсaloriesAmountToConsume(Float kiloсaloriesAmountToConsume) {
        this.kiloсaloriesAmountToConsume = kiloсaloriesAmountToConsume;
    }

    public Float getKiloсaloriesConsumed() {
        return kiloсaloriesConsumed;
    }

    public void setKiloсaloriesConsumed(Float kiloсaloriesConsumed) {
        this.kiloсaloriesConsumed = kiloсaloriesConsumed;
    }

    public Float getWaterAmountToConsume() {
        return waterAmountToConsume;
    }

    public void setWaterAmountToConsume(Float waterAmountToConsume) {
        this.waterAmountToConsume = waterAmountToConsume;
    }

    public Float getWaterConsumed() {
        return waterConsumed;
    }

    public void setWaterConsumed(Float waterConsumed) {
        this.waterConsumed = waterConsumed;
    }

    public Float getProteinAmountToConsume() {
        return proteinAmountToConsume;
    }

    public void setProteinAmountToConsume(Float proteinAmountToConsume) {
        this.proteinAmountToConsume = proteinAmountToConsume;
    }

    public Float getProteinConsumed() {
        return proteinConsumed;
    }

    public void setProteinConsumed(Float proteinConsumed) {
        this.proteinConsumed = proteinConsumed;
    }

    public Float getCarbAmountToConsume() {
        return carbAmountToConsume;
    }

    public void setCarbAmountToConsume(Float carbAmountToConsume) {
        this.carbAmountToConsume = carbAmountToConsume;
    }

    public Float getCarbsConsumed() {
        return carbsConsumed;
    }

    public void setCarbsConsumed(Float carbsConsumed) {
        this.carbsConsumed = carbsConsumed;
    }

    public Float getFatAmountToConsume() {
        return fatAmountToConsume;
    }

    public void setFatAmountToConsume(Float fatAmountToConsume) {
        this.fatAmountToConsume = fatAmountToConsume;
    }

    public Float getFatConsumed() {
        return fatConsumed;
    }

    public void setFatConsumed(Float fatConsumed) {
        this.fatConsumed = fatConsumed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getRegistered() {
        return isRegistered;
    }

    public void setRegistered(Boolean registered) {
        isRegistered = registered;
    }

    public Boolean getEditable() {
        return isEditable;
    }

    public void setEditable(Boolean editable) {
        isEditable = editable;
    }
}
