package com.dev.d1l.healthyfood.utils;

import com.dev.d1l.healthyfood.models.DayStatistic;
import com.dev.d1l.healthyfood.models.User;
import com.dev.d1l.healthyfood.models.request.LoginRequest;
import com.dev.d1l.healthyfood.models.request.ResetPasswordRequest;
import com.dev.d1l.healthyfood.models.request.SignUpRequest;
import com.dev.d1l.healthyfood.models.response.CategoriesResponse;
import com.dev.d1l.healthyfood.models.response.DishesResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("api/Account/Login")
    Call<User> login(@Body LoginRequest request);

    @POST("api/Account/Register")
    Call<String> signUp(@Body SignUpRequest request);

    @POST("api/Account/RecoveryPassword")
    Call<String> resetPassword(@Body ResetPasswordRequest request);

    @GET("oapi/DishCategories")
    Call<CategoriesResponse> getCategories();

    @GET("oapi/Dishes")
    Call<DishesResponse> getDishes();

    @POST("api/MainScreenData")
    Call<List<DayStatistic>> getDaysStatistics(@Header("Content-Type") String contentType, @Body LoginRequest request);

}
