package com.dev.d1l.healthyfood.listeners;

import android.widget.SeekBar;
import android.widget.TextView;

import com.dev.d1l.healthyfood.DishDetailsActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by d1l on 5/2/17.
 */

public class DishSeekBarListener implements SeekBar.OnSeekBarChangeListener {

    DishDetailsActivity activity;
    String additionalInfo;
    int minValue;
    Map<String, Integer> tvMap = new HashMap<>();

    public DishSeekBarListener(DishDetailsActivity activity, String additionalInfo,
                               Integer minValue, Integer maxValue) {
        this.activity = activity;
        this.additionalInfo = additionalInfo;
        this.minValue = minValue;

        tvMap.put("Carbohydrates", Integer.parseInt(activity.carbohydratesValue.getText().toString()
                .replace(additionalInfo, "")));
        tvMap.put("Fats", Integer.parseInt(activity.fatsValue.getText().toString()
                .replace(additionalInfo, "")));
        tvMap.put("Proteins", Integer.parseInt(activity.proteinsValue.getText().toString()
                .replace(additionalInfo, "")));
        tvMap.put("Total", 1);
        tvMap.put("Kcal", Integer.parseInt(activity.kcalValue.getText().toString().replace("kcal", "")));

        Integer max = (maxValue - minValue);
        this.activity.seekBar.setMax(max);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        this.activity.mValue.setText(String.valueOf(seekBar.getProgress()) + additionalInfo);
        this.recalculateValue(activity.carbohydratesValue, tvMap.get("Carbohydrates"), progress, additionalInfo);
        this.recalculateValue(activity.fatsValue, tvMap.get("Fats"), progress, additionalInfo);
        this.recalculateValue(activity.kcalValue, tvMap.get("Kcal"), progress, "");
        this.recalculateValue(activity.proteinsValue, tvMap.get("Proteins"), progress, additionalInfo);
    }

    private void recalculateValue(TextView tv, int value, int k, String additionalInfo) {
        k += this.minValue;
        tv.setText(String.valueOf(value * k / 100) + additionalInfo);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
