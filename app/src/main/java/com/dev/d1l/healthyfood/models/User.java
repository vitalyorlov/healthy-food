package com.dev.d1l.healthyfood.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by d1l on 5/1/17.
 */

public class User implements Serializable {
    @SerializedName("Email")
    private String email;
    @SerializedName("PhoneNumber")
    private String phone;
    @SerializedName("LastName")
    private String lastname;
    @SerializedName("FirstName")
    private String firstname;
    @SerializedName("DateOfBirth")
    private String dateOfBirth;
    @SerializedName("SexName")
    private String sexName;
    @SerializedName("Password")
    private String password;
    @SerializedName("ConfirmPassword")
    private String confirmPassword;
    @SerializedName("Height")
    private Float height;
    @SerializedName("Weight")
    private Float weight;
    @SerializedName("DestinationWeight")
    private Float destinationWeight;
    @SerializedName("WristGirth")
    private Float wristGirth;
    @SerializedName("WaistGirth")
    private Float waistGirth;
    @SerializedName("ThighGirth")
    private Float thighGirth;
    @SerializedName("UserActivityType")
    private String userActivityType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSexName() {
        return sexName;
    }

    public void setSexName(String sexName) {
        this.sexName = sexName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getDestinationWeight() {
        return destinationWeight;
    }

    public void setDestinationWeight(Float destinationWeight) {
        this.destinationWeight = destinationWeight;
    }

    public Float getWristGirth() {
        return wristGirth;
    }

    public void setWristGirth(Float wristGirth) {
        this.wristGirth = wristGirth;
    }

    public Float getWaistGirth() {
        return waistGirth;
    }

    public void setWaistGirth(Float waistGirth) {
        this.waistGirth = waistGirth;
    }

    public Float getThighGirth() {
        return thighGirth;
    }

    public void setThighGirth(Float thighGirth) {
        this.thighGirth = thighGirth;
    }

    public String getUserActivityType() {
        return userActivityType;
    }

    public void setUserActivityType(String userActivityType) {
        this.userActivityType = userActivityType;
    }
}
