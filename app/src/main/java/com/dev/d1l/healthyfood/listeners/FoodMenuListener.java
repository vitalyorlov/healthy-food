package com.dev.d1l.healthyfood.listeners;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.dev.d1l.healthyfood.DishesActivity;
import com.dev.d1l.healthyfood.R;

/**
 * Created by d1l on 5/5/17.
 */

public class FoodMenuListener implements View.OnClickListener {

    private String category;
    private Activity activity;

    public FoodMenuListener(String category, Activity activity) {
        this.category = category;
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        final LinearLayout shadowLayout = (LinearLayout) activity.findViewById(R.id.black_layout);
        shadowLayout.setVisibility(View.GONE);

        LinearLayout breakfast = (LinearLayout) activity.findViewById(R.id.food_breakfast);
        LinearLayout lunch = (LinearLayout) activity.findViewById(R.id.food_lunch);
        LinearLayout dinner = (LinearLayout) activity.findViewById(R.id.food_dinner);
        LinearLayout meal = (LinearLayout) activity.findViewById(R.id.food_meal);

        breakfast.setVisibility(View.GONE);
        lunch.setVisibility(View.GONE);
        dinner.setVisibility(View.GONE);
        meal.setVisibility(View.GONE);

        Animation animBreakfastSlide = AnimationUtils.loadAnimation(activity.getApplicationContext(),
                R.anim.click_dish);
        Animation animLunchSlide = AnimationUtils.loadAnimation(activity.getApplicationContext(),
                R.anim.click_dish);
        Animation animDinnerSlide = AnimationUtils.loadAnimation(activity.getApplicationContext(),
                R.anim.click_dish);
        Animation animMealSlide = AnimationUtils.loadAnimation(activity.getApplicationContext(),
                R.anim.click_dish);

        breakfast.startAnimation(animBreakfastSlide);
        lunch.startAnimation(animLunchSlide);
        dinner.startAnimation(animDinnerSlide);
        meal.startAnimation(animMealSlide);

        Intent intent = new Intent(activity, DishesActivity.class);
        intent.putExtra("category", category);
        activity.startActivity(intent);
    }
}
