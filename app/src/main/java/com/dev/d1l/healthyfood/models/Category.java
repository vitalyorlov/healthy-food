package com.dev.d1l.healthyfood.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by d1l on 5/1/17.
 */

public class Category implements Serializable {
    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
