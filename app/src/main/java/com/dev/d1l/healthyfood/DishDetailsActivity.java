package com.dev.d1l.healthyfood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.d1l.healthyfood.listeners.DishSeekBarListener;
import com.dev.d1l.healthyfood.listeners.SeekBarListener;
import com.dev.d1l.healthyfood.models.Dish;
import com.dev.d1l.healthyfood.models.request.SignUpRequest;

public class DishDetailsActivity extends AppCompatActivity {
    public Dish dish;
    public TextView dishName;
    public TextView kcalValue;
    public TextView proteinsValue;
    public TextView fatsValue;
    public TextView carbohydratesValue;
    public TextView proteinsKey;
    public TextView fatsKey;
    public TextView carbohydratesKey;
    public TextView mValue;
    public SeekBar seekBar;
    public Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_details);

        Intent intent = getIntent();
        dish = (Dish) intent.getSerializableExtra("dish");
        String mode = intent.getStringExtra("mode");
        if (mode == "edit") {
            Button b = (Button) findViewById(R.id.add_button);
            b.setText("- Delete");
        }

        dishName = (TextView) findViewById(R.id.dish_name);
        dishName.setText(dish.getName());
        kcalValue = (TextView) findViewById(R.id.kcal_value);
        kcalValue.setText(String.valueOf(dish.getTotalKcal()));

        View proteinsView = findViewById(R.id.proteins_et);
        proteinsKey = (TextView) proteinsView.findViewById(R.id.key);
        proteinsKey.setText(getResources().getString(R.string.proteins));
        proteinsValue = (TextView) proteinsView.findViewById(R.id.value);
        proteinsValue.setText(String.valueOf(dish.getTotalProteins()) +
                getResources().getString(R.string.gramm));

        View fatsView = findViewById(R.id.fats_et);
        fatsKey = (TextView) fatsView.findViewById(R.id.key);
        fatsKey.setText(getResources().getString(R.string.fats));
        fatsValue = (TextView) fatsView.findViewById(R.id.value);
        fatsValue.setText(String.valueOf(dish.getTotalFats()) +
                getResources().getString(R.string.gramm));

        View carbohydratesView = findViewById(R.id.carbohydrates_et);
        carbohydratesKey = (TextView) carbohydratesView.findViewById(R.id.key);
        carbohydratesKey.setText(getResources().getString(R.string.fats));
        carbohydratesValue = (TextView) carbohydratesView.findViewById(R.id.value);
        carbohydratesValue.setText(String.valueOf(dish.getTotalCarbohydrates()) +
                getResources().getString(R.string.gramm));
//        ImageButton mBackButton = (ImageButton) findViewById(R.id.back_button);
//        mBackButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) { DishDetailsActivity.super.onBackPressed();
//            }
//        });

        addButton = (Button) findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Item has added", Toast.LENGTH_SHORT).show();
                DishDetailsActivity.super.onBackPressed();
                ((Button)view).setText("- Delete");
            }
        });

        seekBar = (SeekBar) findViewById(R.id.heightSB);
        mValue = (TextView) findViewById(R.id.dish_value);
        seekBar.setOnSeekBarChangeListener(new DishSeekBarListener(this,"g", 20, 400));

    }

    private DishDetailsActivity getActivity() {
        return this;
    }
}
