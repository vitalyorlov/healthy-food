package com.dev.d1l.healthyfood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.dev.d1l.healthyfood.R;
import com.dev.d1l.healthyfood.adapters.DishesRvAdapter;
import com.dev.d1l.healthyfood.constants.SharedPrefParams;
import com.dev.d1l.healthyfood.models.Dish;
import com.dev.d1l.healthyfood.models.response.DishesResponse;
import com.dev.d1l.healthyfood.tasks.SaveDataTask;
import com.dev.d1l.healthyfood.utils.ApiInterface;
import com.dev.d1l.healthyfood.utils.CacheService;
import com.dev.d1l.healthyfood.utils.RestService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DishesFragment extends Fragment {

    private RecyclerView rv;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    //private List<Dish> categories;
    private DishesResponse dr;

    public DishesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dishes, container, false);

        rv = (RecyclerView) v.findViewById(R.id.dishes_rv);

        mLayoutManager = new LinearLayoutManager(this.getContext());
        rv.setLayoutManager(mLayoutManager);

        DishesResponse dishesResponse = (DishesResponse)
                CacheService.loadData(getActivity(), SharedPrefParams.DISHES, new DishesResponse());

        if (dishesResponse != null && dishesResponse.getDishes() != null) {
            dr = dishesResponse;
            initRv(dishesResponse);
        } else {
            ApiInterface apiService = RestService.getInstance();
            Call<DishesResponse> call = apiService.getDishes();
            call.enqueue(new DishesRequest());
        }

        final EditText search = (EditText) this.getActivity().findViewById(R.id.search);
        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                DishesResponse ndr = new DishesResponse();
                ndr.setDishes(new ArrayList<Dish>());

                for (Dish d : dr.getDishes()) {
                    if (d.getName().toString().toLowerCase().contains(s.toString().toLowerCase())) {
                        ndr.getDishes().add(d);
                    }
                }

                initRv(ndr);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        return v;
    }

    private void initRv(DishesResponse dishesResponse) {
        mAdapter = new DishesRvAdapter(dishesResponse.getDishes(), this.getActivity());
        rv.setAdapter(mAdapter);
        rv.setHasFixedSize(true);
    }

    private class DishesRequest implements Callback<DishesResponse> {

        @Override
        public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
            int code = response.code();
            if (code == 200) {
                DishesResponse dishesResponse = response.body();
                dr = dishesResponse;
                SaveDataTask task = new SaveDataTask(getActivity(),
                        SharedPrefParams.DISHES, dishesResponse);
                task.execute();
                initRv(dishesResponse);
            } else {
                Toast.makeText(getActivity(),
                        "Error getting dishes: " + String.valueOf(code), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(Call<DishesResponse> call, Throwable t) {
            Toast.makeText(getActivity(), "Connection error", Toast.LENGTH_LONG).show();
        }
    }

}
