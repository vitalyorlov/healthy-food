package com.dev.d1l.healthyfood.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by d1l on 5/7/17.
 */
public class Dish implements Serializable {
    @SerializedName("@odata.type")
    private String dataType;
    @SerializedName("CarbsPercent")
    private Float carbohydratesPercent;
    @SerializedName("ProteinsPercent")
    private Float proteinsPercent;
    @SerializedName("FatPercent")
    private Float fatsPercent;
    @SerializedName("Id")
    private Integer id;
    @SerializedName("Name")
    private String name;
    @SerializedName("Description")
    private String description;
    @SerializedName("TotalProteins")
    private Integer totalProteins;
    @SerializedName("TotalFat")
    private Integer totalFats;
    @SerializedName("TotalCarbs")
    private Integer totalCarbohydrates;
    @SerializedName("TotalCcal")
    private Integer totalKcal;
    @SerializedName("DishCategory")
    private Category dishCategory;
    @SerializedName("DishType")
    private Category dishType;

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Float getCarbohydratesPercent() {
        return carbohydratesPercent;
    }

    public void setCarbohydratesPercent(Float carbohydratesPercent) {
        this.carbohydratesPercent = carbohydratesPercent;
    }

    public Float getProteinsPercent() {
        return proteinsPercent;
    }

    public void setProteinsPercent(Float proteinsPercent) {
        this.proteinsPercent = proteinsPercent;
    }

    public Float getFatsPercent() {
        return fatsPercent;
    }

    public void setFatsPercent(Float fatsPercent) {
        this.fatsPercent = fatsPercent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTotalProteins() {
        return totalProteins;
    }

    public void setTotalProteins(Integer totalProteins) {
        this.totalProteins = totalProteins;
    }

    public Integer getTotalFats() {
        return totalFats;
    }

    public void setTotalFats(Integer totalFats) {
        this.totalFats = totalFats;
    }

    public Integer getTotalCarbohydrates() {
        return totalCarbohydrates;
    }

    public void setTotalCarbohydrates(Integer totalCarbohydrates) {
        this.totalCarbohydrates = totalCarbohydrates;
    }

    public Integer getTotalKcal() {
        return totalKcal;
    }

    public void setTotalKcal(Integer totalKcal) {
        this.totalKcal = totalKcal;
    }

    public Category getDishCategory() {
        return dishCategory;
    }

    public void setDishCategory(Category dishCategory) {
        this.dishCategory = dishCategory;
    }

    public Category getDishType() {
        return dishType;
    }

    public void setDishType(Category dishType) {
        this.dishType = dishType;
    }
}
