package com.dev.d1l.healthyfood;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.d1l.healthyfood.listeners.SeekBarListener;
import com.dev.d1l.healthyfood.models.request.SignUpRequest;

import java.util.Calendar;

import fr.ganfra.materialspinner.MaterialSpinner;

public class PersonalParametersActivity extends AppCompatActivity {
    private SignUpRequest request;
    private View mProgressView;
    private View mLoginFormView;
    private SeekBar mHeightSeekBar;
    private SeekBar mWeightSeekBar;
    private SeekBar mTargetWeightSeekBar;
    private SeekBar mWristGirthSeekBar;
    private SeekBar mWaistGirthSeekBar;
    private SeekBar mThighGirthSeekBar;
    private TextView mHeightValue;
    private TextView mWeightValue;
    private TextView mTargetWeightValue;
    private TextView mWristGirthValue;
    private TextView mWaistGirstValue;
    private TextView mThighGirthValue;
    private MaterialSpinner mSpinner;
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_parameters);

        Intent intent = getIntent();
        request = (SignUpRequest) intent.getSerializableExtra("request");

        ImageButton mBackButton = (ImageButton) findViewById(R.id.back_button);
        mBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PersonalParametersActivity.super.onBackPressed();
            }
        });

        mHeightSeekBar = (SeekBar) findViewById(R.id.height);
        mHeightValue = (TextView) findViewById(R.id.heightResult);
        mHeightSeekBar.setOnSeekBarChangeListener(new SeekBarListener(mHeightSeekBar, mHeightValue,
                "cm", 80, 280));

        mWeightSeekBar = (SeekBar) findViewById(R.id.weight);
        mWeightValue = (TextView) findViewById(R.id.weightResult);
        mWeightSeekBar.setOnSeekBarChangeListener(new SeekBarListener(mWeightSeekBar, mWeightValue,
                "kg", 20, 300));

        mTargetWeightSeekBar = (SeekBar) findViewById(R.id.targetWeight);
        mTargetWeightValue = (TextView) findViewById(R.id.targetWeightResult);
        mTargetWeightSeekBar.setOnSeekBarChangeListener(new SeekBarListener(mTargetWeightSeekBar,
                mTargetWeightValue,
                "kg", 20, 300));

        mWristGirthSeekBar = (SeekBar) findViewById(R.id.wristGirth);
        mWristGirthValue = (TextView) findViewById(R.id.wristGirthResult);
        mWristGirthSeekBar.setOnSeekBarChangeListener(new SeekBarListener(mWristGirthSeekBar, mWristGirthValue,
                "cm", 1, 30));

        mWaistGirthSeekBar = (SeekBar) findViewById(R.id.waistGirth);
        mWaistGirstValue = (TextView) findViewById(R.id.waistGirthResult);
        mWaistGirthSeekBar.setOnSeekBarChangeListener(new SeekBarListener(mWaistGirthSeekBar, mWaistGirstValue,
                "cm", 30, 300));

        mThighGirthSeekBar = (SeekBar) findViewById(R.id.thighGirth);
        mThighGirthValue = (TextView) findViewById(R.id.thighGirthResult);
        mThighGirthSeekBar.setOnSeekBarChangeListener(new SeekBarListener(mThighGirthSeekBar,
                mThighGirthValue,
                "cm", 20, 200));

        String[] ITEMS = {
                getResources().getString(R.string.male),
                getResources().getString(R.string.female)
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner = (MaterialSpinner) findViewById(R.id.gender);
        mSpinner.setAdapter(adapter);

        dateView = (TextView) findViewById(R.id.birthDate);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR) - 18;
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);

        Button mNextButton = (Button) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) { goNext();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void goNext() {
        request.setSexName(mSpinner.getSelectedItem().toString());
        request.setHeight((float)mHeightSeekBar.getProgress());
        request.setWeight((float)mWeightSeekBar.getProgress());
        request.setDestinationWeight((float)mTargetWeightSeekBar.getProgress());
        request.setWaistGirth((float)mWaistGirthSeekBar.getProgress());
        request.setWristGirth((float)mWristGirthSeekBar.getProgress());
        request.setThighGirth((float)mThighGirthSeekBar.getProgress());

        Intent intent = new Intent(this, PersonalActivityActivity.class);
        intent.putExtra("request", request);
        startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    @SuppressWarnings("deprecation")
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker,
                                      int year, int month, int day) {
                    showDate(year, month + 1, day);
                }
            };

    private void showDate(int year, int month, int day) {
        StringBuilder date = new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year);
        dateView.setText(date);
        request.setDateOfBirth(date.toString());
    }
}

