package com.dev.d1l.healthyfood.adapters;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.d1l.healthyfood.DishDetailsActivity;
import com.dev.d1l.healthyfood.R;
import com.dev.d1l.healthyfood.models.Dish;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;

import java.util.ArrayList;
import java.util.List;

public class RationDishesRvAdapter extends RecyclerView.Adapter<RationDishesRvAdapter.DishesViewHolder> {

    public static  final int[] MY_COLORS = {
            Color.parseColor("#f1c40f"), Color.parseColor("#9b59b6"),
            Color.parseColor("#16a085"), Color.parseColor("#e74c3c")
    };

    public static class DishesViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout;
        PieChart chart;
        TextView dishName;
        int dishId;

        DishesViewHolder(View itemView) {
            super(itemView);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.dishDataLayout);
            chart = (PieChart)itemView.findViewById(R.id.dishChart);
            dishName = (TextView)itemView.findViewById(R.id.dishText);
        }
    }

    List<Dish> dishes;
    Activity context;

    public RationDishesRvAdapter(List<Dish> dishes, Activity context){
        this.dishes = dishes;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public DishesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dish_item, viewGroup, false);
        DishesViewHolder dvh = new DishesViewHolder(v);
        return dvh;
    }

    @Override
    public void onBindViewHolder(final DishesViewHolder dishesViewHolder, int i) {
        dishesViewHolder.dishName.setText(dishes.get(i).getName());
        dishesViewHolder.dishId = i;
        initChart(dishesViewHolder.chart);
        setDataForPieChart(dishesViewHolder.chart, dishes.get(i));
        dishesViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DishDetailsActivity.class);
                intent.putExtra("mode", "edit");
                intent.putExtra("dish", dishes.get(dishesViewHolder.dishId));
                context.startActivity(intent);
            }
        });
    }

    private void initChart(PieChart mChart) {
        mChart.setCenterText("210");
        mChart.setCenterTextColor(Color.WHITE);
        mChart.setCenterTextSize(13);
        mChart.setHoleColor(Color.TRANSPARENT);
        mChart.setDescription(null);
        mChart.setHoleRadius(90);
        mChart.setTransparentCircleRadius(100);
        Legend legend = mChart.getLegend();
        legend.setEnabled(false);
    }

    public void setDataForPieChart(PieChart mChart, Dish dish) {
        ArrayList<PieEntry> yValues = new ArrayList<PieEntry>();
        yValues.add(new PieEntry(dish.getProteinsPercent()));
        yValues.add(new PieEntry(dish.getFatsPercent()));
        yValues.add(new PieEntry(dish.getCarbohydratesPercent()));
        yValues.add(new PieEntry(100 - dish.getCarbohydratesPercent()
                - dish.getProteinsPercent() - dish.getFatsPercent()));

        PieDataSet dataSet = new PieDataSet(yValues, "");
        dataSet.setSliceSpace(0);
        dataSet.setSelectionShift(2);

        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int c : MY_COLORS)
            colors.add(c);
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);

        data.setValueFormatter(new DefaultValueFormatter(3));
        data.setValueTextSize(0f);
        data.setValueTextColor(Color.WHITE);

        mChart.setData(data);
        mChart.highlightValues(null);

        mChart.invalidate();

        //mChart.animateXY(1400, 1400);

    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

}