package com.dev.d1l.healthyfood;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dev.d1l.healthyfood.adapters.SectionsPagerAdapter;
import com.dev.d1l.healthyfood.fragments.ContentMainFragment;
import com.dev.d1l.healthyfood.fragments.SupportFragment;
import com.dev.d1l.healthyfood.fragments.UserActivityFragment;
import com.dev.d1l.healthyfood.fragments.UserDataFragment;
import com.dev.d1l.healthyfood.listeners.FoodMenuListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private boolean isFoodMenuShowed = false;
    private LinearLayout breakfast;
    private LinearLayout lunch;
    private LinearLayout dinner;
    private LinearLayout meal;
    private Fragment fragment;
    private FloatingActionButton fab;
    private Fragment contentMainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contentMainFragment = new ContentMainFragment();
        setFragment(contentMainFragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.add_new_dish);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        initNavigationView();

        breakfast = (LinearLayout) findViewById(R.id.food_breakfast);
        lunch = (LinearLayout) findViewById(R.id.food_lunch);
        dinner = (LinearLayout) findViewById(R.id.food_dinner);
        meal = (LinearLayout) findViewById(R.id.food_meal);

    }

    private void initNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_main);
    }

    private void showMenu() {
        isFoodMenuShowed = true;
        final LinearLayout shadowLayout = (LinearLayout) findViewById(R.id.black_layout);
        shadowLayout.setVisibility(View.VISIBLE);

        breakfast.setVisibility(View.VISIBLE);
        lunch.setVisibility(View.VISIBLE);
        dinner.setVisibility(View.VISIBLE);
        meal.setVisibility(View.VISIBLE);

        Animation animBreakfastSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_1);
        Animation animLunchSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_2);
        Animation animDinnerSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_3);
        Animation animMealSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_4);

        breakfast.startAnimation(animBreakfastSlide);
        lunch.startAnimation(animLunchSlide);
        dinner.startAnimation(animDinnerSlide);
        meal.startAnimation(animMealSlide);

        breakfast.setOnClickListener(new FoodMenuListener("Breakfast", this));
        lunch.setOnClickListener(new FoodMenuListener("Lunch", this));
        dinner.setOnClickListener(new FoodMenuListener("Dinner", this));
        meal.setOnClickListener(new FoodMenuListener("Meal", this));

    }

    private void hideMenu() {
        isFoodMenuShowed = false;
        final LinearLayout shadowLayout = (LinearLayout) findViewById(R.id.black_layout);

        Animation animBreakfastSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_1_out);
        Animation animLunchSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_2_out);
        Animation animDinnerSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_3_out);
        Animation animMealSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_dish_4_out);

        breakfast.startAnimation(animBreakfastSlide);
        lunch.startAnimation(animLunchSlide);
        dinner.startAnimation(animDinnerSlide);
        meal.startAnimation(animMealSlide);

        breakfast.setVisibility(View.GONE);
        lunch.setVisibility(View.GONE);
        dinner.setVisibility(View.GONE);
        meal.setVisibility(View.GONE);
        shadowLayout.setVisibility(View.GONE);
    }

    @Override
    public void onResume(){
        super.onResume();

        breakfast.setVisibility(View.GONE);
        lunch.setVisibility(View.GONE);
        dinner.setVisibility(View.GONE);
        meal.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (isFoodMenuShowed) {
            hideMenu();
        } else  {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_ration) {
            Intent intent = new Intent(this, RationActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private MainActivity getContext() {
        return this;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_main) {
            Intent intent = new Intent(getContext(), MainActivity.class);
            getContext().startActivity(intent);
        } else if (id == R.id.nav_support) {
            fragment = new SupportFragment();
            fab.setVisibility(View.GONE);
            setFragment(fragment);
        } else if (id == R.id.nav_user_activity) {
            fragment = new UserActivityFragment();
            fab.setVisibility(View.GONE);
            setFragment(fragment);
        } else if (id == R.id.nav_user_data) {
            fragment = new UserDataFragment();
            fab.setVisibility(View.GONE);
            setFragment(fragment);
        } else if (id == R.id.nav_logout) {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            getContext().startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
        }
    }
}
